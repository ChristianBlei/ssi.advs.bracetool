﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Autodesk.AdvanceSteel.Arrangement;
using Autodesk.AdvanceSteel.CADAccess;
using Autodesk.AdvanceSteel.CADLink.Database;

using Autodesk.AdvanceSteel.Connection;
using Autodesk.AdvanceSteel.ConstructionHelper;
using Autodesk.AdvanceSteel.ConstructionTypes;
using Autodesk.AdvanceSteel.DocumentManagement;
using Autodesk.AdvanceSteel.Geometry;
using Autodesk.AdvanceSteel.Modelling;
using Autodesk.AdvanceSteel.Modeler;
using Autodesk.AdvanceSteel.Profiles;
//using Autodesk.AdvanceSteel.Runtime;
using Autodesk.AdvanceSteel.DotNetRoots;
using Autodesk.AdvanceSteel.DerivedDocuments;
using Autodesk.AdvanceSteel.Services;
using Autodesk.AdvanceSteel.Contours;

using AsGeometry = Autodesk.AdvanceSteel.Geometry;
using AsDatabase = Autodesk.AdvanceSteel.CADLink.Database.Database;
using AsObjectId = Autodesk.AdvanceSteel.CADLink.Database.ObjectId;
using AsDocument = Autodesk.AdvanceSteel.DocumentManagement.Document;
using AsTransactionManager = Autodesk.AdvanceSteel.CADAccess.TransactionManager;
using AsTransaction = Autodesk.AdvanceSteel.CADAccess.Transaction;

using Autodesk.AutoCAD;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.DatabaseServices;

using AcDocument = Autodesk.AutoCAD.ApplicationServices.Document;
using AcObjectId = Autodesk.AutoCAD.DatabaseServices.ObjectId;
using AcTransaction = Autodesk.AutoCAD.DatabaseServices.Transaction;
using AcGeometry = Autodesk.AutoCAD.Geometry;
using AcApplication = Autodesk.AutoCAD.ApplicationServices.Application;
using Autodesk.AutoCAD.Runtime;


[assembly: ExtensionApplication(typeof(SSI.AdvS.BraceTool.AddIn))]
[assembly: CommandClass(typeof(SSI.AdvS.BraceTool.AddIn))]

namespace SSI.AdvS.BraceTool
{
   public class AddIn : IExtensionApplication

   {
   

      public void Initialize()
      {
         Console.WriteLine("SSI.ADVS.BraceTool is loaded");
      }

      public void Terminate()
      {

      }


      [CommandMethod("SSI", "SSI_RoundFlat",null, CommandFlags.Modal)]
      public void RoundFlat()
      {
      FilterFlat.GetFlat();   

      }

   }
}
