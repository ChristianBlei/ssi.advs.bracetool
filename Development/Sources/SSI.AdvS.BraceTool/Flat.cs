﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Autodesk.AdvanceSteel.Arrangement;
using Autodesk.AdvanceSteel.CADAccess;
using Autodesk.AdvanceSteel.CADLink.Database;
using AsObjectId = Autodesk.AdvanceSteel.CADLink.Database.ObjectId;
using AsTransactionManager = Autodesk.AdvanceSteel.CADAccess.TransactionManager;
using AsTransaction = Autodesk.AdvanceSteel.CADAccess.Transaction;
using AsDatabase = Autodesk.AdvanceSteel.CADLink.Database.Database;
using Autodesk.AdvanceSteel.Connection;
using Autodesk.AdvanceSteel.ConstructionHelper;
using Autodesk.AdvanceSteel.ConstructionTypes;
using Autodesk.AdvanceSteel.DocumentManagement;
using asDocument = Autodesk.AdvanceSteel.DocumentManagement.Document;
using AsGeometry = Autodesk.AdvanceSteel.Geometry;
using AsCamera = Autodesk.AdvanceSteel.ConstructionHelper.Camera;
using Autodesk.AdvanceSteel.Modelling;
using Autodesk.AdvanceSteel.Modeler;
using Autodesk.AdvanceSteel.Profiles;
using Autodesk.AdvanceSteel.Runtime;
using Autodesk.AdvanceSteel.DotNetRoots;
using Autodesk.AdvanceSteel.DerivedDocuments;
using Autodesk.AdvanceSteel.Services;
using Autodesk.AdvanceSteel.Contours;
using Autodesk.AdvanceSteel.DotNetRoots.DatabaseAccess;

using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.DatabaseServices;


using AcApplication = Autodesk.AutoCAD.ApplicationServices.Application;
using AcApplicationServices = Autodesk.AutoCAD.ApplicationServices;
using AcCoreApplication = Autodesk.AutoCAD.ApplicationServices.Core.Application;
using AcDocument = Autodesk.AutoCAD.ApplicationServices.Document;
using AcObjectId = Autodesk.AutoCAD.DatabaseServices.ObjectId;
using AcTransaction = Autodesk.AutoCAD.DatabaseServices.Transaction;
using AcDatabase = Autodesk.AutoCAD.DatabaseServices.Database;
using AcOpenFileDialog = Autodesk.AutoCAD.Windows.OpenFileDialog;
using AcOpenFileOrFolderDialog = Autodesk.AutoCAD.Windows.OpenFileOrFolderDialog;
using Autodesk.AutoCAD.GraphicsInterface;
using Autodesk.AutoCAD.BoundaryRepresentation;
using AsTransActionManager = Autodesk.AdvanceSteel.CADAccess.TransactionManager;
using AcGeometry = Autodesk.AutoCAD.Geometry;


namespace SSI.AdvS.BraceTool
{

  public class Flat
  {

    IReadOnlyList<FeatureObject> features;

    public Flat(AsObjectId asObjectId, StraightBeam beam)
    {
      AsObjId = asObjectId;
      SelectedFlat = beam;
      if (CheckFlat())
      {

      }
    }

    public AsObjectId AsObjId { get; private set; }
    public StraightBeam SelectedFlat { get; private set; }



    private bool CheckFlat()
    {
      bool isValid = false;

      IEnumerable<AsObjectId> featIds = SelectedFlat.GetFeatures(false);
      if (featIds.Where(f => DatabaseManager.Open(f).IsKindOf(FeatureObject.eObjectType.kConnectionHoleFeature)).ToList().Count == 2 && featIds.Where(f => !DatabaseManager.Open(f).IsKindOf(FeatureObject.eObjectType.kConnectionHoleFeature)).ToList().Count == 0)
      {
        Debug.Print("");
        isValid = true;
      }
      else
      {
        MessageBox.Show("Flat has illegal features");
      }

      return isValid;
    }

    private bool CheckHoles(IEnumerable<AsObjectId> featIds)
    {
      bool isValid = false;

      // Check, if holefields are single holes, if diameter is legal, position is legal and if holes have correct position to ends


      return isValid;
    }



  }

}
