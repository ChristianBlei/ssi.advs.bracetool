﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Autodesk.AdvanceSteel.Arrangement;
using Autodesk.AdvanceSteel.CADAccess;
using Autodesk.AdvanceSteel.CADLink.Database;
using AsObjectId = Autodesk.AdvanceSteel.CADLink.Database.ObjectId;
using AsTransactionManager = Autodesk.AdvanceSteel.CADAccess.TransactionManager;
using AsTransaction = Autodesk.AdvanceSteel.CADAccess.Transaction;
using AsDatabase = Autodesk.AdvanceSteel.CADLink.Database.Database;
using Autodesk.AdvanceSteel.Connection;
using Autodesk.AdvanceSteel.ConstructionHelper;
using Autodesk.AdvanceSteel.ConstructionTypes;
using Autodesk.AdvanceSteel.DocumentManagement;
using asDocument = Autodesk.AdvanceSteel.DocumentManagement.Document;
using AsGeometry = Autodesk.AdvanceSteel.Geometry;
using AsCamera = Autodesk.AdvanceSteel.ConstructionHelper.Camera;
using Autodesk.AdvanceSteel.Modelling;
using Autodesk.AdvanceSteel.Modeler;
using Autodesk.AdvanceSteel.Profiles;
using Autodesk.AdvanceSteel.Runtime;
using Autodesk.AdvanceSteel.DotNetRoots;
using Autodesk.AdvanceSteel.DerivedDocuments;
using Autodesk.AdvanceSteel.Services;
using Autodesk.AdvanceSteel.Contours;
using Autodesk.AdvanceSteel.DotNetRoots.DatabaseAccess;

using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.DatabaseServices;


using AcApplication = Autodesk.AutoCAD.ApplicationServices.Application;
using AcApplicationServices = Autodesk.AutoCAD.ApplicationServices;
using AcCoreApplication = Autodesk.AutoCAD.ApplicationServices.Core.Application;
using AcDocument = Autodesk.AutoCAD.ApplicationServices.Document;
using AcObjectId = Autodesk.AutoCAD.DatabaseServices.ObjectId;
using AcTransaction = Autodesk.AutoCAD.DatabaseServices.Transaction;
using AcDatabase = Autodesk.AutoCAD.DatabaseServices.Database;
using AcOpenFileDialog = Autodesk.AutoCAD.Windows.OpenFileDialog;
using AcOpenFileOrFolderDialog = Autodesk.AutoCAD.Windows.OpenFileOrFolderDialog;
using Autodesk.AutoCAD.GraphicsInterface;
using Autodesk.AutoCAD.BoundaryRepresentation;
using AsTransActionManager = Autodesk.AdvanceSteel.CADAccess.TransactionManager;
using AcGeometry = Autodesk.AutoCAD.Geometry;


namespace SSI.AdvS.BraceTool
{
  public static class FilterFlat
  {

    public static void GetFlat()
    {
      AsObjectId asObjectId = UserInteraction.SelectObject();
      if (asObjectId != null)
      {
        using (DocumentAccess documentAccess = new DocumentAccess())
        {
          FilerObject filerObject = DatabaseManager.Open(asObjectId);
          if (filerObject != null && filerObject.IsKindOf(FilerObject.eObjectType.kStraightBeam))
          {
            StraightBeam straightBeam = filerObject as StraightBeam;
            AsGeometry.Point3d startPt = straightBeam.GetPointAtStart();
            AsGeometry.Point3d endPt = straightBeam.GetPointAtEnd();
            straightBeam.GetBodyLengthDeltas(out double start, out double end);                                       //??
            straightBeam.GetLengthOffset(out double bstart, out double bend, BodyContext.eBodyContext.kNormal);

            ProfileType profileType = straightBeam.GetProfType();
            if (profileType.Type == ProfileType.eProfType.F)
            {
              double width = profileType.GetGeometricalData(ProfileType.eCommonData.kWidth);
              double height = profileType.GetGeometricalData(ProfileType.eCommonData.kProfHeight);
              if ((height == 50 || height == 40 || height == 30) && width == 5)
              {
                Flat flat = new Flat(asObjectId, straightBeam);
                ContourEnd contour = new ContourEnd(25);

                if (height < 50)
                {

                  BeamMultiContourNotch notch = new BeamMultiContourNotch(flat.SelectedFlat, Beam.eEnd.kEnd, contour.CPoly, new AsGeometry.Vector3d(0, 0, 1), new AsGeometry.Vector3d(1, 0, 0));
                  notch.WriteToDb();
                                    flat.SelectedFlat.AddFeature(notch);
                }
                else
                {
                  BeamFeatEdge edge1 = new BeamFeatEdge(flat.SelectedFlat, new AsGeometry.Point3d(0, height / 2,0), FilerObject.eFilletTypes.kFilletConvex);
                
                  edge1.WriteToDb();
                  edge1.Radius = height / 2;
                  flat.SelectedFlat.AddFeature(edge1);
                  BeamFeatEdge edge2 = new BeamFeatEdge(flat.SelectedFlat, new AsGeometry.Point3d(0, -height / 2, 0), FilerObject.eFilletTypes.kFilletConvex);
                  edge2.WriteToDb();
                  edge2.Radius = height / 2;
                  flat.SelectedFlat.AddFeature(edge2);
                }


              }

            }
          }

          documentAccess.Commit();
        }
      }


    }


  }

}
