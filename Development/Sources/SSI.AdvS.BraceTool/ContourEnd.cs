﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Autodesk.AdvanceSteel.Arrangement;
using Autodesk.AdvanceSteel.CADAccess;
using Autodesk.AdvanceSteel.CADLink.Database;
using AsObjectId = Autodesk.AdvanceSteel.CADLink.Database.ObjectId;
using AsTransactionManager = Autodesk.AdvanceSteel.CADAccess.TransactionManager;
using AsTransaction = Autodesk.AdvanceSteel.CADAccess.Transaction;
using AsDatabase = Autodesk.AdvanceSteel.CADLink.Database.Database;
using Autodesk.AdvanceSteel.Connection;
using Autodesk.AdvanceSteel.ConstructionHelper;
using Autodesk.AdvanceSteel.ConstructionTypes;
using Autodesk.AdvanceSteel.DocumentManagement;
using asDocument = Autodesk.AdvanceSteel.DocumentManagement.Document;
using AsGeometry = Autodesk.AdvanceSteel.Geometry;
using AsCamera = Autodesk.AdvanceSteel.ConstructionHelper.Camera;
using Autodesk.AdvanceSteel.Modelling;
using Autodesk.AdvanceSteel.Modeler;
using Autodesk.AdvanceSteel.Profiles;
using Autodesk.AdvanceSteel.Runtime;
using Autodesk.AdvanceSteel.DotNetRoots;
using Autodesk.AdvanceSteel.DerivedDocuments;
using Autodesk.AdvanceSteel.Services;
using Autodesk.AdvanceSteel.Contours;
using Autodesk.AdvanceSteel.DotNetRoots.DatabaseAccess;

using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.DatabaseServices;


using AcApplication = Autodesk.AutoCAD.ApplicationServices.Application;
using AcApplicationServices = Autodesk.AutoCAD.ApplicationServices;
using AcCoreApplication = Autodesk.AutoCAD.ApplicationServices.Core.Application;
using AcDocument = Autodesk.AutoCAD.ApplicationServices.Document;
using AcObjectId = Autodesk.AutoCAD.DatabaseServices.ObjectId;
using AcTransaction = Autodesk.AutoCAD.DatabaseServices.Transaction;
using AcDatabase = Autodesk.AutoCAD.DatabaseServices.Database;
using AcOpenFileDialog = Autodesk.AutoCAD.Windows.OpenFileDialog;
using AcOpenFileOrFolderDialog = Autodesk.AutoCAD.Windows.OpenFileOrFolderDialog;
using Autodesk.AutoCAD.GraphicsInterface;
using Autodesk.AutoCAD.BoundaryRepresentation;
using AsTransActionManager = Autodesk.AdvanceSteel.CADAccess.TransactionManager;
using AcGeometry = Autodesk.AutoCAD.Geometry;

namespace SSI.AdvS.BraceTool
{
  public class ContourEnd
  {

    public ContourEnd()
    { 
    
    }

    public ContourEnd(double radius)
    {
      Radius = radius;
      CPoly = CreatePoly();
      Contour = CreateContour();
    }

    public double Radius { get;private set; }
    public AsGeometry.Polyline3d CPoly { get; set; }
    public BeamMultiContourNotch Contour { get; set; }


    private AsGeometry.Polyline3d CreatePoly()
    {
      List<AsGeometry.Point3d> vertices = new List<AsGeometry.Point3d>();
      vertices.Add(new AsGeometry.Point3d(0, 0, 0));
      vertices.Add(new AsGeometry.Point3d(-Radius,-Radius, 0));
      vertices.Add(new AsGeometry.Point3d(Radius/2, -Radius, 0));
      vertices.Add(new AsGeometry.Point3d(Radius / 2, Radius, 0));
      vertices.Add(new AsGeometry.Point3d(-Radius, Radius, 0));  

      List<AsGeometry.VertexInfo> vertexInfos = new List<AsGeometry.VertexInfo>();
      vertexInfos.Add(new AsGeometry.VertexInfo(Radius,new AsGeometry.Point3d(-Radius,0,0),new AsGeometry.Vector3d(0,0,-1)));
      vertexInfos.Add(new AsGeometry.VertexInfo());
      vertexInfos.Add(new AsGeometry.VertexInfo());
      vertexInfos.Add(new AsGeometry.VertexInfo());
      vertexInfos.Add(new AsGeometry.VertexInfo(Radius, new AsGeometry.Point3d(-Radius, 0, 0), new AsGeometry.Vector3d(0, 0, -1)));

      //AsGeometry.Polyline3d poly = new AsGeometry.Polyline3d(vertices.ToArray(), null, true, true);
      AsGeometry.Polyline3d poly = new AsGeometry.Polyline3d(vertices.ToArray(), vertexInfos.ToArray(), true,true);
      return poly;
    }

    private BeamMultiContourNotch   CreateContour()
    {
      BeamMultiContourNotch contour = new BeamMultiContourNotch();


    return contour;
    }


  }
}
